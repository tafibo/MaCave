
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var minifyHtml = require('gulp-minify-html');
const {phpMinify} = require('@cedx/gulp-php-minify');
var gulp = require('gulp');

var source = './src/';
var paths = {
  js   : source + 'js/*.js',
  scss : source + 'scss/*.scss',
  html : source + '*.html',
  php : source + 'backend/**/*.php'
};


gulp.task('php', () => gulp.src(paths.php, {read: false})
  .pipe(phpMinify({binary: 'C:\\Users\\tafibo\\AppData\\Local\\xampp\\php\\php.exe'}))
  .pipe( gulp.dest( './backend/' ) )
);


gulp.task('html', function() {
  return gulp.src(paths.html)
          .pipe(minifyHtml())
          .pipe( gulp.dest( './' ) )
});

gulp.task('scripts', function() {
  return gulp.src(paths.js)
          .pipe( uglify() )
          .pipe( gulp.dest( './js' ) )
});

gulp.task('styles', function () {
  return gulp.src(paths.scss)
    .pipe(sass.sync().on('error', sass.logError))
    .pipe( sass( { outputStyle: 'compressed' } ) )
    .pipe(gulp.dest('./css'));
});

/*
gulp.task('watch', function(){
    gulp.watch(paths.js).on('change', ['scripts']);
    gulp.watch(paths.scss).on('change', ['styles']);
    gulp.watch(paths.html).on('change', ['html']);
});
*/