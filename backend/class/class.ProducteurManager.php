<?php
declare(strict_types = 1); require_once 'class/class.Producteur.php'; class ProducteurManager { private $_oMySql; const TABLE_NAME = "d_producteur"; public function __construct() { $this->_oMySql = $GLOBALS["_oMySql"] ; } public function getProducteurlist() { $ProducteurList = array(); $query = "select id_Producteur, Nom, Prenom, Adresse, CP, Ville, Telephone, Horaire, Site_web, Email
				 from ".self::TABLE_NAME; $Result = $this->_oMySql->query($query); if (!$Result) { trigger_error("Erreur dans l'execution de la requête", E_USER_WARNING); } $j = 0; while($d = $Result->fetch()) { $ProducteurList[$j] = new Producteur($d[0], $d[1], $d[2], $d[3], $d[4], $d[5], $d[6], $d[7], $d[8], $d[9]); $j++; } return $ProducteurList; } public function getProducteurId(string $NomProducteur) { $query = "select id_Producteur
					from ".self::TABLE_NAME."
					where Nom_producteur = '".$NomProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['id_Producteur']; } public function getProducteurNom(int $IdProducteur) { $query = "select nom
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['nom']; } public function getProducteurPrenom(int $IdProducteur) { $query = "select Prenom
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['Prenom']; } public function getProducteurAdresse(int $IdProducteur) { $query = "select Adresse
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['Adresse']; } public function getProducteurCP(int $IdProducteur) { $query = "select CP
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['CP']; } public function getProducteurVille(int $IdProducteur) { $query = "select Ville
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['Ville']; } public function getProducteurTelephone(int $IdProducteur) { $query = "select Telephone
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['Telephone']; } public function getProducteurHoraire(int $IdProducteur) { $query = "select Horaire
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['Horaire']; } public function getProducteurWeb(int $IdProducteur) { $query = "select Site_web
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['Site_web']; } public function getProducteurEmail(int $IdProducteur) { $query = "select Email
					from ".self::TABLE_NAME."
					where id_producteur = '".$IdProducteur."'"; $resultat = $this->_oMySql->query($query); return $resultat->fetch()['Email']; } public function removeProducteur(int $IdProducteur) { $query = "delete from ".self::TABLE_NAME."
					where id_Producteur = '".$IdProducteur."'"; return $this->_oMySql->query($query); } public function createProducteur(Producteur $producteur) { $query = "insert into ".self::TABLE_NAME." (Nom, Prenom, Adresse, CP, Ville, Telephone, Horaire, Site_web, Email) 
						values ('".$producteur->getProducteurNom()."','".$producteur->getProducteurPrenom()."','".$producteur->getProducteurAdresse()."','" .$producteur->getProducteurCP()."','".$producteur->getProducteurVille()."','".$producteur->getProducteurTelephone()."',
						'".$producteur->getProducteurHoraire()."','".$producteur->getProducteurWeb()."','".$producteur->getProducteurEmail()."')"; print $query; return $this->_oMySql->query($query); } public function updateProducteur(Producteur $producteur) { $query = "update ".self::TABLE_NAME." set ". "Nom = '".$producteur->getProducteurNom()."',". "Prenom = '".$producteur->getProducteurPrenom()."',". "Adresse = '".$producteur->getProducteurAdresse()."',". "CP = '".$producteur->getProducteurCP()."',". "Ville = '".$producteur->getProducteurVille()."',". "Telephone = '".$producteur->getProducteurTelephone()."',". "Horaire = '".$producteur->getProducteurHoraire()."',". "Site_web = '".$producteur->getProducteurWeb()."',". "Email = '".$producteur->getProducteurEmail()."'". " where id_Producteur=".$producteur->getProducteurId(); print $query; try { $statement = $this->_oMySql->prepare($query); $result = $statement->execute(); return $result; } catch (Exception $e) { die("Query test error : ".$e->getMessage()); } } function __destruct() { unset($this->_oMySql); } } ?>