"use strict";

function AddLine(div, divError) {
	//var date = new Date();
	
	$('#forms').append('<form id="Addline" name="Addline"></form>');
	
	var html = ''
				+ '<tr class="row100">'
				+ '<td headers="Nom" id="Nom" class="column100 column1" data-column="column1">'
				+ 	'<input form="Addline" name="nom" type=text required />'
				+ '</td>'
				+ '<td headers="Prénom" id="prenom" class="column100 column2" data-column="column2">'
				+ 	'<input form="Addline" name="prenom" type=text />'
				+ '</td>'
				+ '<td headers="Adresse" id="adresse" class="column100 column3" data-column="column3">'
				+ 	'<input form="Addline" name="adresse" type=text required />'
				+ '</td>'
				+ '<td headers="Code Postal" id="CP" class="column100 column4" data-column="column4">'
				+ 	'<input form="Addline" name="cp" type=text required />'
				+ '</td>'
				+ '<td headers="Ville" class="column100 column5" data-column="column5">'
				+ 	'<input form="Addline" name="ville" type=text required />'
				+ '</td>'
				+ '<td headers="Téléphone" class="column100 column6" data-column="column6">'
				+ 	'<input form="Addline" name="telephone" type=text required />'
				+ '</td>'
				+ '<td headers="Horaire" class="column100 column7" data-column="column7">'
				+ 	'<input form="Addline" name="horaire" type=text />'
				+ '</td>'
				+ '<td headers="Site" class="column100 column8" data-column="column8">'
				+ 	'<input form="Addline" name="site" type=text />'
				+ '</td>'
				+ '<td headers="Email" class="column100 column9" data-column="column9">'
				+ 	'<input form="Addline" name="email" type=text />'
				+ '</td>'
				+ '<td class="column100 column99">'
				+ 	'<input type="submit" form="Addline" value="Save" onclick="SendForms(\'#Addline\', \'backend/AddProducteur.php?type=add\', \'#msg\');"   class="btn btn-dark btn-xs" >'
				+ '</td>'
				+ '<td class="column100 column99"></td>'
				+ '</tr>';
	$(div).append(html);

};

function getForms (div, url, divError) {
	var jqxhr = $.getJSON( url)
			.done(function(data) {
				var LineLoop = 0;
				var ColLoop = 0;
				var id="table";
				var html = '', prenom = '', nom = '', adresse = '', cp = '';
				var ville = '', horaire = '', telephone = '', site = '', email = '';

				$.each(data, function () {
					
					$('#forms').append('<form  name="' + data[LineLoop].id +'" id="'+ data[LineLoop].id + '"></form>');
					LineLoop++;
				});
				html = html + '<table data-vertable="ver5" id="'+id+'"><thead><tr class="row100 head">';
				html = html + '<th class="column100 column1" data-column="column1">Nom*</th>';
				html = html + '<th class="column100 column2" data-column="column2">Prénom</th>';
				html = html + '<th class="column100 column3" data-column="column3">Adresse*</th>';
				html = html + '<th class="column100 column4" data-column="column4">Code Postal*</th>';
				html = html + '<th class="column100 column5" data-column="column5">Ville*</th>';
				html = html + '<th class="column100 column6" data-column="column6">Téléphone*</th>';
				html = html + '<th class="column100 column7" data-column="column7">Horaire</th>';
				html = html + '<th class="column100 column7" data-column="column8">Site</th>';
				html = html + '<th class="column100 column7" data-column="column9">Email</th>';
				html = html + '<th class="column100 column8"></th>';
				html = html + '<th></th>';

				html = html + '</tr></thead><tbody>';

				//Pour chaque ligne du json (1 formulaire)
				$.each( data, function( ) {

					html = html + '<tr class="row100"><td class="column100 column1" data-column="column1">';
					//id
					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=hidden ' + 
						' name="id"' + 
						' value="' + data[ColLoop].id + '"/>';

					//Nom
					nom=(!data[ColLoop].nom)?"":data[ColLoop].nom;
					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="nom"' + 
						' value="' + nom + '"/>';

					//Prénom
					html = html + '</td><td class="column100 column2" data-column="column2">';
					prenom=(!data[ColLoop].prenom)?"":data[ColLoop].prenom;

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="prenom"' + 
						' value="' + prenom + '"/>';

					//adresse
					html = html + '</td><td class="column100 column3" data-column="column3">';
					adresse=(!data[ColLoop].adresse)?"":data[ColLoop].adresse;

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="adresse"' + 
						' value="' + adresse + '"/>';
					
					//Code postal
					html = html + '</td><td class="column100 column4" data-column="column4">';
					cp=(!data[ColLoop].cp)?"":data[ColLoop].cp;

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="cp"' + 
						' value="' + cp + '"/>';
					
					html = html + '</td><td class="column100 column5" data-column="column5">';
					ville=(!data[ColLoop].ville)?"":data[ColLoop].ville;
					
					//ville
					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="ville"' + 
						' value="' + ville + '"/>';

					//téléphone
					html = html + '</td><td class="column100 column6" data-column="column6">';
					telephone=(!data[ColLoop].telephone)?"":data[ColLoop].telephone;
					
					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="telephone"' + 
						' value="' + telephone + '"/>';

					//horaire
					html = html + '</td><td class="column100 column7" data-column="column7">';
					horaire=(!data[ColLoop].horaire)?"":data[ColLoop].horaire;

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="horaire"' + 
						' value="' + horaire + '"/>';

					//site
					html = html + '</td><td class="column100 column7" data-column="column7">';
					site=(!data[ColLoop].site)?"":data[ColLoop].site;

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="site"' + 
						' value="' + site + '"/>';

					//horaire
					html = html + '</td><td class="column100 column7" data-column="column7">';
					email=(!data[ColLoop].email)?"":data[ColLoop].email;

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type=text ' + 
						' name="email"' + 
						' value="' + email + '"/>';

					//bouton
					html = html + '</td>';
					html = html + '<td class="column100 column99">';

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type="submit" '+ 
						' value="Save" '+
						' onclick="SendForms(\'#' + data[ColLoop].id +'\', \'backend/addProducteur.php?type=update\', \'#msg\')" ' +
						' class="btn btn-dark btn-xs" />';

					html = html + '</td>';	
					html = html + '<td class="column100 column99">';

					html = html + '<input form="' + data[ColLoop].id + '"' +
						' type="submit" '+ 
						' value="delete" '+
						' onclick="SendForms(\'#' + data[ColLoop].id +'\', \'backend/addProducteur.php?type=del\', \'#msg\')" ' +
						' class="btn btn-dark btn-xs" />';

					html = html + '</td></tr>';

					ColLoop ++;
				})
				html = html + '<tbody></table>'

				$(div).html(html);

				$('#add').html("<input id=\"AddlineButton\" type=\"button\" onclick=\"AddLine('#table', '#msg');$('#AddlineButton').attr('disabled',true);\" value=\"+\" class=\"btn btn-dark btn-xs\" />");


			})
			.fail(function() {
				$(divError).append("<p style='color:red'>Error in loading data</p><br>");
			})

};