"use strict";


function getOption(type, divError, divTarget) {
	var url="";
	var html="";
	switch (type) {
		case 'Cepage': 
			url="backend/addCepage.php?type=jsonOption";
		break;
		case 'Volume':
			url='backend/addVolume.php?type=jsonOption';
		break;
		case 'Producteur':
			url='backend/addProducteur.php?type=jsonOption';
		break;
		case 'Couleur':
			url='backend/addCouleur.php?type=jsonOption';
		break;
	}
	var jqxhr = $.getJSON( url)
		.done(function(data, textStatus) {
			var i = 0;

			html = html + '<select name="id'+type.toLowerCase()+'" form="Addline">';

			$.each(data, function () {
				html = html + '<option value="'+ data[i]["id"]  +'">'+ data[i]["value"]  +'</option>';
				i++;
  			});
			html = html + '</select>';
			$(divTarget).html(html);
		})

		.fail(function( jqxhr, textStatus, error ) {
    		var err = textStatus + ", " + error;
			$(divError).append("<p style='color:red'>Error in loading data : "+type+"<br>reason:"+err+"</p>");
		})
}

function OpenDiv(select, div, file) {
	if(select=="Nouveau") {
		/*
		$(document).ready(function () {
			var test = {
				id: "div",
				class: "divclass",
				css: {
					"color": "Green"
				}
			};
			var $div = $("<div>", test);
			$div.html("New Division");
			$("body").append($div);
		});*/
		getForms('#corp'+div, file, '#msg');
		$(div+'Div').visibility = "visible";
	}
};



function CloseDiv(div) {
	$(div).visibility = "hidden";
};

function SendForms(id, target, divError) {
	$(id).submit(
   		function(event){
			event.preventDefault();

			var formData = new FormData();
			
			$(":input[form="+id.substring(1)+"]").each(function(){
 				formData.append($(this).attr('name'), $(this).val());
			});

   			$.ajax({
	            type: 'POST',
	            //async:  true,
	            //cache: false,
				processData: false,
				contentType: false,
				url: './'+target,
				data: formData,

	            success: function(msg) {
					$(divError).append("\n" + msg);
					$(divError).append("success <br>");
	            },
	            error: function(msg) {
					$(divError).append("\n" + msg);
					//$(divError).append("error <br>");
	            },
				fail: function(msg) {
					$(divError).append("\n" + msg);
					$(divError).append("fail <br>");
	            },
				complete: function (msg) {
					$(divError).append(msg);
					//$(divError).append("complete <br>");
					
					setTimeout(function() {
                     location.reload();
                  	}, 1000);
				}
			   });
			   
   			
   		}
   	);
    return false;
};      










