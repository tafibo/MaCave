"use strict";

function AddLine(div, divError) {	
	$('#forms').append('<form id="Addline" name="Addline"></form>');
	
	var html = ''
				+ '<tr class="row100">'
				+ '<td headers="nom" id="nom" class="column100 column1" data-column="column1">'
				+ 	'<input form="Addline" name="nom" type=text required />'
				+ '</td>'
				+ '<td class="column100 column99">'
				+ 	'<input type="submit" form="Addline" value="Save" onclick="SendForms(\'#Addline\', \'backend/AddCouleur.php?type=add&return=couleur\', \'#msg\');"   class="btn btn-dark btn-xs" >'
				+ '</td>'
				+ '<td class="column100 column99"></td>'
				+ '</tr>';
	$(div).append(html);

};

function getForms (div, url, divError) {
	var jqxhr = $.getJSON( url)
			.done(function(data) {
				var LineLoop = 0;
				var ColLoop = 0;
				var id="table";
				var html = ' ';
                var nom = '';

				$.each(data, function () {
					
					$('#forms').append('<form  name="' + data[LineLoop].id +'" id="'+ data[LineLoop].id + '"></form>');
					LineLoop++;
				});
				html = html + '<table data-vertable="ver5" id="'+id+'"><thead><tr class="row100 head">';
				html = html + '<th class="column100 column1" data-column="column1">Nom*</th>';
				html = html + '<th class="column100 column8"></th>';
				html = html + '<th></th>';

				html = html + '</tr></thead><tbody>';

				//Pour chaque ligne du json (1 formulaire)
				$.each( data, function( ) {

					html = html + '<tr class="row100"><td class="column100 column1" data-column="column1">';
					//Nom
					nom=(!data[ColLoop].nom)?"":data[ColLoop].nom;
					html = html + '<input form="' + data[ColLoop].id + 
						'" type=text ' + 
						' name="nom" value="' + nom + '"/>';

					html = html + '<input form="' + data[ColLoop].id + 
						'" type=hidden ' + 
						' name="id" value="' + data[ColLoop].id + '"/>';

					//bouton
					html = html + '</td>';
					html = html + '<td class="column100 column99">';

					html = html + '<input form="' + data[ColLoop].id + 
						'" type="submit" '+ 
						' value="Save" '+
						' onclick="SendForms(\'#' + data[ColLoop].id +'\', \'backend/addCouleur.php?type=update&return=couleur\', \'#msg\')" ' +
						' class="btn btn-dark btn-xs" />';

					html = html + '</td>';	
					html = html + '<td class="column100 column99">';

					html = html + '<input form="' + data[ColLoop].id + 
						'" type="submit" '+ 
						' value="delete" '+
						' onclick="SendForms(\'#' + data[ColLoop].id +'\', \'backend/addCouleur.php?type=del&return=couleur\', \'#msg\')" ' +
						' class="btn btn-dark btn-xs" />';

					html = html + '</td></tr>';

					ColLoop ++;
				})
				html = html + '<tbody></table>'

				$(div).html(html);

				$('#add').html("<input id=\"AddlineButton\" type=\"button\" onclick=\"AddLine('#table', '#msg');$('#AddlineButton').attr('disabled',true);\" value=\"+\" class=\"btn btn-dark btn-xs\" />");


			})
			.fail(function() {
				$(divError).append("<p style='color:red'>Error in loading data</p><br>");
			})

};