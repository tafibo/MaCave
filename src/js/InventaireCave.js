"use strict";

function AddLine(div, divError) {
	var date = new Date();
	
	$('#forms').append('<form id="Addline" name="Addline"></form>');
	
	var html = ''
				+ '<tr class="row100">'
				+ '<td headers="Cepage" id="addCepage" class="column100 column1" data-column="column1"></td>'
				+ '<td headers="Couleur" id="addCouleur" class="column100 column2" data-column="column2"></td>'
				+ '<td headers="Volume" id="addVolume" class="column100 column3" data-column="column3"></td>'
				+ '<td headers="Producteur" id="addProducteur" class="column100 column4" data-column="column4"></td>'
				+ '<td headers="Annee" class="column100 column5" data-column="column5"><input form="Addline" name="Annee" style="width:50px;" type=text required value="'+date.getFullYear()+'"/></td>'
				+ '<td headers="AnneeConso" class="column100 column6" data-column="column6"><input form="Addline" name="AnneeConso" style="width:50px;" type=text /></td>'
				+ '<td headers="Quantite" class="column100 column7" data-column="column7"><input form="Addline" name="quantite" style="width:50px;" type=number value=0 /></td>'
				+ '<td class="column100 column8"><input type="submit" form="Addline" value="Save" onclick="SendForms(\'#Addline\', \'backend/InventaireCave.php?type=add\', \'#msg\');"   class="btn btn-dark btn-xs" ></td>'
				+ '<td class="column100 column8"></td>'
				+ '</tr>';
	$(div).append(html);

	getOption('Cepage', divError, "#addCepage");
	getOption('Couleur', divError, "#addCouleur");
	getOption('Volume', divError, "#addVolume");
	getOption('Producteur', divError, "#addProducteur");

};

function getForms (div, url, divError) {
	// Assign handlers immediately after making the request,
	// and remember the jqxhr object for this request
	var jqxhr = $.getJSON( url)
			.done(function(data) {
				var a = 0;
				var i = 0;
				var html = '';
				var id="table";

				$.each(data, function () {
					
					$('#forms').append('<form  name="' + data[a].id +'" id="'+ data[a].id + '"></form>');
					a++;
				});
	
				html = html + '<table data-vertable="ver5" id="'+id+'"><thead><tr class="row100 head">';
				html = html + '<th class="column100 column1" data-column="column1">Cépage</th>';
				html = html + '<th class="column100 column2" data-column="column2">Couleur</th>';
				html = html + '<th class="column100 column3" data-column="column3">Volume</th>';
				html = html + '<th class="column100 column4" data-column="column4">Producteur</th>';
				html = html + '<th class="column100 column5" data-column="column5">Année de production</th>';
				html = html + '<th class="column100 column6" data-column="column6">Année de Consomation</th>';
				html = html + '<th class="column100 column7" data-column="column7">Quantité</th>';
				html = html + '<th class="column100 column8"></th>';
				html = html + '<th></th>';

				html = html + '</tr></thead><tbody>';

				//Pour chaque ligne du json (1 formulaire)
				$.each( data, function( ) {

					html = html + '<tr class="row100"><td class="column100 column1" data-column="column1">';
					//Cépage
					html = html + '<input form="' + data[i].id + '"' +
						' type="' + data[i].cepage.type + '"' +
						' name="' + data[i].cepage.name + '"' +
						' value="' + data[i].cepage.value + '"/>' +
						data[i].cepage.label;

					//id
					html = html + '<input form="' + data[i].id + '"' +
						' type=hidden ' + 
						' name="id" ' + 
						' value="' + data[i].id + '"/>';

					//Couleur
					html = html + '</td><td class="column100 column2" data-column="column2">';

					html = html + '<input form="' + data[i].id + '"' +
						' type="' + data[i].couleur.type + '"' +
						' name="' + data[i].couleur.name + '"' +
						' value="' + data[i].couleur.value + '"/>' +
						data[i].couleur.label;

					//Volume
					html = html + '</td><td class="column100 column3" data-column="column3">';

					html = html + '<input form="' + data[i].id + '"' +
						' type="' + data[i].volume.type + '"' +
						' name="' + data[i].volume.name + '"' +
						' value="' + data[i].volume.value + '"/>' +
						data[i].volume.label;
					
					//Producteur
					html = html + '</td><td class="column100 column4" data-column="column4">';

					html = html + '<input form="' + data[i].id + '"' +
						' type="' + data[i].producteur.type + '"' +
						' name="' + data[i].producteur.name + '"' +
						' value="' + data[i].producteur.value + '"/>' +
						data[i].producteur.label;

					html = html + '</td><td class="column100 column5" data-column="column5">';

					//Année de production	
					html = html + '<input form="' + data[i].id + '"' +
						' type="' + data[i].annee.type + '"' +
						' name="' + data[i].annee.name + '"' +
						' value="' + data[i].annee.value + '"/>' +
						data[i].annee.label;

					//Année de Consomation
					html = html + '</td><td class="column100 column6" data-column="column6">';

					html = html + '<input form="' + data[i].id + '"' +
						' type="' + data[i].anneeconso.type + '"' +
						' name="' + data[i].anneeconso.name + '"' +
						' value="' + data[i].anneeconso.value + '" />' + 
						data[i].anneeconso.label;

					//Quantité
					html = html + '</td><td class="column100 column7" data-column="column7">';

					html = html + '<input form="' + data[i].id + '"' +
						' type="' + data[i].quantite.type + '"' +
						' name="' + data[i].quantite.name + '"' +
						' value="' + data[i].quantite.value + '"/>';

					html = html + '</td>';	
					html = html + '<td class="column100 column99">';

					html = html + '<input form="' + data[i].id + '"' +
						' type="submit" '+ 
						' value="Save" '+
						' onclick="SendForms(\'#' + data[i].id +'\', \'backend/InventaireCave.php?type=update\', \'#msg\')" ' +
						' class="btn btn-dark btn-xs" />';

					html = html + '</td>';	
					html = html + '<td class="column100 column99">';

					html = html + '<input form="' + data[i].id + '"' +
						' type="submit" '+ 
						' value="delete" '+
						' onclick="SendForms(\'#' + data[i].id +'\', \'backend/InventaireCave.php?type=del\', \'#msg\')" ' +
						' class="btn btn-dark btn-xs" />';

					html = html + '</td></tr>';

					i = i + 1;
				})
				html = html + '<tbody></table>'

				$(div).html(html);
				
				$("#add").html("<input id=\"AddlineButton\" type=\"button\" onclick=\"AddLine('#table', '#msg');$('#AddlineButton').attr('disabled',true);\" value=\"+\" class=\"btn btn-dark btn-xs\" />");
			})
			.fail(function() {
				$(divError).append("<p style='color:red'>Error in loading data</p>");
			})


};