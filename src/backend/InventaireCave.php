<?php
declare(strict_types = 1);

require_once 'config.php';
require_once 'fonction.php';

require_once 'class/class.Cave.php';
require_once 'class/class.CaveManager.php';

$idbouteille = -1;	
$idcepage    = -1;
$idcouleur 	 = -1;	
$idvolume 	 = -1;
$idproducteur= -1;
$idAnnee 	 = -1;
$idAnneeConso= -1;
$NbBouteille = -1;	
$type		 = "";

$type 			= isset($_GET["type"])? 			htmlentities($_GET["type"]):"";
$idbouteille	= isset($_POST["id"])? 				(int)trim(htmlentities($_POST["id"])):-1;
$idcepage 		= isset($_POST["idcepage"])? 		(int)trim(htmlentities($_POST["idcepage"])):-1;
$idcouleur 		= isset($_POST["idcouleur"])? 		(int)trim(htmlentities($_POST["idcouleur"])):-1;
$idvolume 		= isset($_POST["idvolume"])? 		(int)trim(htmlentities($_POST["idvolume"])):-1;
$idproducteur 	= isset($_POST["idproducteur"])? 	(int)trim(htmlentities($_POST["idproducteur"])):-1;
$idAnnee 		= isset($_POST["Annee"])? 			(int)trim(htmlentities($_POST["Annee"])):-1;
$idAnneeConso 	= isset($_POST["idAnneeConso"])? 	(int)trim(htmlentities($_POST["idAnneeConso"])):-1;
$NbBouteille	= isset($_POST["quantite"])? 		(int)trim(htmlentities($_POST["quantite"])):-1;

$CaveM = new CaveManager();
$result = false;

switch($type) {
	case "json":
		$json = array();
		$CaveListes = $CaveM->getCaveList ();
		
		for($i = 0; $i < count ( $CaveListes ); $i ++) {
			$cave = $CaveListes [$i];
			
			$json[$i] = array(
				"id"			=> $cave->getBouteilleId(),
				"name"			=>	"InventaireCave",
				"cepage"		=>  array(
										"id"		=> "idcepage",
										"name"		=> "idcepage",
										"value" 	=> $cave->getCepageO()->getCepageId(),
										"type"		=> "hidden",
										"label"		=> $cave->getCepageO()->getCepageName(),
										"caption"	=> "Cépage"
									),
				"couleur"		=>	array(
										"id"		=> "idcouleur",
										"name"		=> "idcouleur",
										"value" 	=> $cave->getCouleurO()->getCouleurId(),
										"type"		=> "hidden",
										"label" 	=> $cave->getCouleurO()->getCouleurName(),
										"caption"	=> "Couleur"
									),	
				"volume"		=>	array(
										"id"		=> "idvolume",
										"name"		=> "idvolume",
										"value" 	=> $cave->getVolumeO()->getVolumeId(),
										"type"		=> "hidden",
										"label" 	=> $cave->getVolumeO()->getVolumeName(),
										"caption"	=> "Volume"
									),
				"producteur"	=>	array(
										"id"		=> "idproducteur",
										"name"		=> "idproducteur",
										"value" 	=> $cave->getProducteurO()->getProducteurId(),
										"type"		=> "hidden",
										"label" 	=> $cave->getProducteurO()->getProducteurPrenom()." ".$cave->getProducteurO()->getProducteurNom(),
										"caption"	=> "Producteur"
									),
				"annee"			=>	array(
										"id"		=> "idAnnee",
										"name"		=> "idAnnee",
										"value" 	=> $cave->getAnnee(),
										"type"		=> "hidden",
										"label" 	=> $cave->getAnnee(),
										"caption"	=> "Année de production"
									),
				"anneeconso"	=>	array(
										"id"		=> "idAnneeConso",
										"name"		=> "idAnneeConso",
										"value" 	=> (is_null($cave->getAnneeConso())?"":$cave->getAnneeConso()),
										"type"		=> "hidden",
										"label" 	=> (is_null($cave->getAnneeConso())?"":$cave->getAnneeConso()),
										"caption"	=> "Année de consomation"
									),
				"quantite"		=>	array(
										"name"		=> "quantite",
										"value" 	=> $cave->getNbBouteille(),
										"type"		=> "number",
										"caption"	=> "Quantité de bouteille"
									)
				);
		}

		print json_encode($json);
		unset($json);
	break;
	case "update":
		try  {
			$cave = new Cave($idbouteille, $idproducteur, $idcepage, 
							$idcouleur, $idvolume, $idAnnee, $NbBouteille, 
							$idAnneeConso, $idReview = 0);

			$result = $CaveM->updateCave($cave);
			return $result;
		}  catch (Exception $e) {
			die("Query error : ".$e->getMessage());
		}
	break;
	case "add":

		
		if ($idproducteur==-1 || $idcepage==-1 ||  $idcouleur==-1 || $idvolume==-1 || $idAnnee==-1 || $NbBouteille==-1) {
			print "Error ID";
		} else {
			try {	
				$cave = new Cave(0,
								(int)$idproducteur, 
								(int)$idcepage, 
								(int)$idcouleur, 
								(int)$idvolume, 
								(int)$idAnnee, 
								(int)$NbBouteille, 
								(int)$idAnneeConso = 0, 
								(int)$idReview = 0);
				
				$result = $CaveM->CreateCave($cave);
				return $result;
			}  catch (Exception $e) {
				die("Query error : ".$e->getMessage());
			}
		}
	break;
	case "del":
		if ($idbouteille==-1) {
			print "Error ID";
		} else {
			try  {	
				$cave = new Cave((int)$idbouteille,
								(int)$idproducteur, 
								(int)$idcepage, 
								(int)$idcouleur, 
								(int)$idvolume, 
								(int)$idAnnee, 
								(int)$NbBouteille, 
								(int)$idAnneeConso = 0, 
								(int)$idReview = 0);
				
				$result = $CaveM->removeBottle($cave);
			
				return $result;
			}  catch (Exception $e) {
				die("Query error : ".$e->getMessage());	
			}
		}
	break;
}

	

?>
