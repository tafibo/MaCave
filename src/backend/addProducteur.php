<?php
require_once 'config.php';
require_once 'fonction.php';

require_once 'class/class.Producteur.php'; 
require_once 'class/class.ProducteurManager.php';

$id 		= isset($_POST["id"])		?(int)htmlentities($_POST["id"]):0;
$nom 		= isset($_POST["nom"])		?htmlentities($_POST["nom"]):"";
$prenom 	= isset($_POST["prenom"])	?htmlentities($_POST["prenom"]):"";
$adresse 	= isset($_POST["adresse"])	?htmlentities($_POST["adresse"]):"";
$cp 		= isset($_POST["cp"])		?htmlentities($_POST["cp"]):"";
$ville 		= isset($_POST["ville"])	?htmlentities($_POST["ville"]):"";
$telephone 	= isset($_POST["telephone"])?htmlentities($_POST["telephone"]):"";
$horaire	= isset($_POST["horaire"])	?htmlentities($_POST["horaire"]):"";
$site 		= isset($_POST["site"])		?htmlentities($_POST["site"]):"";
$email 		= isset($_POST["email"])	?htmlentities($_POST["email"]):"";
$type 		= isset($_GET["type"])		?htmlentities($_GET["type"]):"";

$ProducteurO = new ProducteurManager();

switch ($type) {
	case "jsonOption":
		$ProducteurListe = $ProducteurO->getProducteurList();
		for($i = 0; $i < count ( $ProducteurListe ); $i ++) {
			$Prod = $ProducteurListe [$i];
			
			$json[$i] = array(
				"id"			=> $Prod->getProducteurId(),
				"value"			=> $Prod->getProducteurPrenom()." ".$Prod->getProducteurNom(),
				"name"			=> "Producteur"
			);		
		}
		print json_encode($json);
	break;
	case "json":
		$ProducteurListe = $ProducteurO->getProducteurList();
		for($i = 0; $i < count ( $ProducteurListe ); $i ++) {
			$Prod = $ProducteurListe[$i];

			$json[$i] = array(
				"id"			=> $Prod->getProducteurId(),
				"name"			=> "Producteur",
				"prenom"		=> $Prod->getProducteurPrenom(),
				"nom" 			=> $Prod->getProducteurNom(),
				"adresse"		=> $Prod->getProducteurAdresse(),
				"cp"			=> $Prod->getProducteurCP(),
				"ville"			=> $Prod->getProducteurVille(),
				"telephone"		=> $Prod->getProducteurTelephone(),
				"horaire"		=> $Prod->getProducteurHoraire(),
				"site"			=> $Prod->getProducteurWeb(),
				"email"			=> $Prod->getProducteurEmail()				
			);	
		}
		print json_encode($json);
	break;
	case "add":
		$Prod = new Producteur($id, $nom, $prenom, $adresse, $cp,
			$ville, $telephone, $horaire, $site, $email, $type);
		
		$ProducteurO->createProducteur($Prod);
	break;
	case "del":
		$ProducteurO->removeProducteur($id);
	break;
	case "update":
		$Prod = new Producteur($id, $nom, $prenom, $adresse, $cp,
			$ville, $telephone, $horaire, $site, $email, $type);
		
		$ProducteurO->updateProducteur($Prod);
	break;
}
?>