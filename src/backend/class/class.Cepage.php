<?php

class Cepage {

	private $_idCepage;
	private $_nomCepage; 

	public function __construct(int $id = 0, $name = "") {
		$this->setCepageId($id);
		$this->setCepageNom($name);
	}

	public function getCepageName() { return $this->_nomCepage; }
	public function getCepageId() { return $this->_idCepage; }

	public function setCepageId(int $IdCepage) { 
		if (is_numeric($IdCepage)) {
			$this->_idCepage = $IdCepage;
		}
	}
	public function setCepageNom($NomCepage) { $this->_nomCepage = $NomCepage; }
	
    public function getAllSelect() {
    }
	
	public function printout() {
		echo $this->_idCepage;
		echo $this->_nomCepage;
	}
	
	function __destruct() {
		unset($this->_idCepage);
		unset($this->_nomCepage);
	}
	
	
}


?>