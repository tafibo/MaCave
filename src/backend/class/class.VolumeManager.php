<?php
require_once 'class/class.Volume.php';
/**
 * 
 * Permet de gerer plusieurs objets de type volume
 *
 */
class VolumeManager {

	private $_oMySql;
	const  TABLE_NAME = "d_volume";

	public function __construct() {
			$this->_oMySql = $GLOBALS["_oMySql"];
	}

	public function getVolumelist() {
		$VolumeList = array();
		
		$query = "select id_volume, Nom_volume from ".self::TABLE_NAME;
		$Result = $this->_oMySql->query($query); 
		
		if (!$Result) {
			trigger_error("Erreur dans l'execution de la requête", E_USER_WARNING);
		}
		$j = 0;

		while($d = $Result->fetch()) {
			$VolumeList[$j] = new Volume($d[0], $d[1]);
			$j++;
		}
		
		return $VolumeList;
		
	}
	
	public function getVolumeId(string $NomVolume) {
		$query = "select id_volume
					from ".self::TABLE_NAME."
					where Nom_volume = '".$NomVolume."'";

		$resultat = $this->_oMySql->query($query);
		return $resultat->fetch()['id_volume'];
	}
	
	public function getVolumeNom($IdVolume) {
		$query = "select Nom_volume
					from ".self::TABLE_NAME."
					where id_volume = '".$IdVolume."'";
	
		$resultat = $this->_oMySql->query($query);
		return $resultat->fetch()['Nom_volume'];
	}

	public function removeVolume($IdVolume) {
		$vol = new Volume($IdVolume);
		
		$query = "delete from ".self::TABLE_NAME."
					where id_volume = '".$vol->getVolumeId()."'";
		
		return $this->_oMySql->query($query);
		/* 
		if ($this->_oMySql->query($query)) {
			$query = "call updateAutoIncrement(".self::TABLE_NAME.")";
			return $this->_oMySql->query($query);
		} */
	}

	public function createVolume(Volume $Volume) {
		$query = "insert into ".self::TABLE_NAME." (Nom_volume) value ('".$Volume->getVolumeName()."')";
		
		return $this->_oMySql->query($query);
	}

	public function updateVolume(Volume $Volume) {
		$query = "update ".self::TABLE_NAME." set ".
					" Nom_volume='".$Volume->getVolumeName()."'".
					" where id_volume=".$Volume->getVolumeId();
		
		return $this->_oMySql->query($query);
	}	
	function __destruct() {
		unset($this->_oMySql);
	}

}

?>