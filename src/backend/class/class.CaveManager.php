<?php
declare(strict_types = 1);

/**
 *
 * Permet de gerer les multiples classes pour créer la cave.
 *
 */
class CaveManager  {

	private $_oMySql;
	const  TABLE_NAME = "f_cave";

	function __construct() {
		$this->_oMySql = $GLOBALS["_oMySql"] ;
	}
	
	function getCaveList():array {
		
		$query = "select id_bouteille, id_producteur, id_cepage, id_couleur, id_volume, Annee, sum(Nombrebouteille) Nombrebouteille, AnneeConso, id_review
					from ".self::TABLE_NAME."
				    group by id_bouteille, id_producteur, id_cepage, id_couleur, id_volume, Annee, AnneeConso, id_review";
		
		$result = $this->_oMySql->query($query) or die(trigger_error("Erreur dans l'execution de la requête :".print_r($this->_oMySql->errorInfo()), E_USER_WARNING));
		
		$j = 0;
		
		while($d = $result->fetch()) {
			
			$CaveList[$j] = new Cave($d['id_bouteille'], 
									 $d['id_producteur'], 
									 $d['id_cepage'], 
									 $d['id_couleur'], 
									 $d['id_volume'], 
									 $d['Annee'], 
									 $d['Nombrebouteille'], 
									 $d['AnneeConso'], 
									 $d['id_review']);
			$j++;
		}

		return $CaveList;
		
	}

	function updateCave(Cave $cave):bool {
		$result = "";

		$query = "Update ".self::TABLE_NAME." 
					set NombreBouteille = ".$cave->getNbBouteille().",
						id_producteur = ".$cave->getProducteurO()->getProducteurId().",  
						id_couleur = ".$cave->getCouleurO()->getCouleurId().", 
						id_cepage = ".$cave->getCepageo()->getCepageId().",
						id_volume = ".$cave->getVolumeO()->getVolumeId().", 
						Annee = ".$cave->getAnnee()."
					where id_bouteille = ".$cave->getBouteilleId();
		try {
			$statement = $this->_oMySql->prepare($query);
			$result = $statement->execute();

			return $result;
		} catch (Exception $e) {
			die("Update error : "+$e->getMessage());
		} 
	}
	
	function CreateCave(Cave $cave):bool {
		$result = "";

		$query = "select count(*) from ".self::TABLE_NAME." where ".
					" id_producteur 	= ".$cave->getProducteurO()->getProducteurId()." and ".
					" id_cepage	  		= ".$cave->getCepageo()->getCepageId()." and ".
					" id_couleur 	  	= ".$cave->getCouleurO()->getCouleurId()." and ".
					" id_volume 	  	= ".$cave->getVolumeO()->getVolumeId();
		
		try {
			$statement = $this->_oMySql->prepare($query);
			$statement->execute();
			$result = $statement->fetch()[0];
		
		} catch (Exception $e) {
			die("Query test error : ".$e->getMessage());
		} 		

		try{
			if ($result<=0) {
				
				$query = "insert into ".self::TABLE_NAME." (id_producteur, id_cepage, id_couleur, id_volume, Annee, NombreBouteille, AnneeConso, id_review) values (
								  	".$cave->getProducteurO()->getProducteurId().",
									".$cave->getCepageo()->getCepageId().",
							 	  	".$cave->getCouleurO()->getCouleurId().", 
							 	  	".$cave->getVolumeO()->getVolumeId().", 
							 		".$cave->getAnnee().",
							 		".$cave->getNbBouteille().",
									".$cave->getAnneeConso().",
									".$cave->getReviewId().")";

				try {
					$statement = $this->_oMySql->prepare($query);
					$result = $statement->execute();

					return $result;
				} catch (Exception $e) {
					die("Insert error : ".$e->getMessage());
				}
			} else  {
				return false;
			}
		} catch (Exception $e) {
			die("result: ".$result."Insert error : ".$e->getMessage());
		}
		return false;
	}

	function removeBottle(Cave $cave):bool {
		$query = "delete from ".self::TABLE_NAME." where id_bouteille= ".$cave->getBouteilleId();
		try {
			$statement = $this->_oMySql->prepare($query);
			$result = $statement->execute();

			return $result;
		} catch  (Exception $e) {
			die("Delete error : ".$e->getMessage());
		}
		
	}
	
}
?>