<?php
declare(strict_types = 1);

require_once 'class/class.Cepage.php';
require_once 'class/class.CepageManager.php';
require_once 'class/class.Couleur.php';
require_once 'class/class.CouleurManager.php';
require_once 'class/class.Volume.php';
require_once 'class/class.VolumeManager.php';
require_once 'class/class.Producteur.php';
require_once 'class/class.ProducteurManager.php';

require_once 'config.php';
/**
 * 
 * Classe de gestion de la cave
 * Gère les liens entre les sous classes (Cepage, Volume, Couleur, Producteur...)
 *
 */

class Cave {
	/* Contient des objets */
	private $_OCave;
	private $_OProducteur;
	private $_OCepage;
	private $_OCouleur;
	private $_OVolume;

	//private $_OReview;
	
	/* Contient des Data */
	private $_idBouteille;
	private $_Annee;
	private $_NbBouteille;
	private $_AnneeConso;
	private $_idReview;
	
	/**
	 * 
	 * Constructeur, utilise par défaut une cave vide.
	 * 
	 * @param number $idBouteille
	 * @param number $idProducteur
	 * @param number $idCepage
	 * @param number $idCouleur
	 * @param number $idVolume
	 * @param number $Annee
	 * @param number $NbBouteille
	 * @param number $AnneeConso
	 * @param number $idReview
	 * 
	 */
	public function __construct($idBouteille = 0, $idProducteur = 0, $idCepage = 0,    $idCouleur = 0,
								$idVolume = 0, 	  $Annee = 1970,     $NbBouteille = 0, $AnneeConso = 1970, 
								$idReview = 0) {

		$this->setBouteilleId((int)$idBouteille);
		$this->setCepageO((int)$idCepage);
		$this->setCouleurO((int)$idCouleur);
		$this->setProducteurO((int)$idProducteur);
		$this->setVolumeO((int)$idVolume);
		$this->setAnnee((int)$Annee);
		$this->setNbBouteille((int)$NbBouteille);
		$this->setAnneeConso((int)$AnneeConso);		
		$this->setReviewO((int)$idReview);
	}
	
	/**
	 * Retourne l'id de la bouteille
	 */
	public function getBouteilleId():int { return $this->_idBouteille; }

	/**
	 * Retourne l'objet Cave
	 */
	public function getCaveO() { return $this->_OCave; }
	
	/**
	 * Retourne l'objet Producteur
	 */
	public function getProducteurO() { return $this->_OProducteur; }
	
	/**
	 * Retourne l'objet Cepage
	 */
	public function getCepageO() { return $this->_OCepage; }
	
	/**
	 * Retourne l'objet Couleur
	 */
	public function getCouleurO() { return $this->_OCouleur; }
	
	/**
	 * Retourne l'objet Volume
	 */
	public function getVolumeO() { return $this->_OVolume; }
	
	/**
	 * Retourne l'annee de mise en bouteille
	 */
	public function getAnnee():int { return $this->_Annee; }
	
	/**
	 * Retourne le nombre de bouteille contenu dans la cave
	 */
	public function getNbBouteille():int { return $this->_NbBouteille; }
	
	/**
	 * Retourne l'Annee de consomation idéale
	 */
	public function getAnneeConso():int { return $this->_AnneeConso; }
	
	/**
	 * Retourne l'objet de commentaire
	 */
	public function getReviewId():int { return $this->_idReview; }
	
	/**
	 * Affecte le nombre de bouteille à la cave
	 * 
	 * @param number $idBouteille
	 */
	public function setBouteilleId(int $idBouteille) {
		if (is_numeric($idBouteille)) {
			$this->_idBouteille = $idBouteille;
		}
	}
	
	/**
	 * Configure le sous objet Cepage avec les informations de la base de données 
	 * (si idCepage existe)
	 * 
	 * @param number $idCepage
	 *
	 */
	public function setCepageO(int $idCepage) {
		if (is_numeric($idCepage)) {
			$cepM = new CepageManager();

			if ($cepM->getCepageNom($idCepage) != "") {
				
				$cep = new Cepage($idCepage, $cepM->getCepageNom($idCepage));
				$this->_OCepage = $cep;
				
			} else {
				$this->_OCepage = null;
			}
			unset($cep);
		}
	}
	
	/**
	 * Configure le sous objet Couleur avec les informations de la base de données 
	 * (si idCouleur existe)
	 * 
	 * @param number $idCouleur
	 */
	public function setCouleurO(int $idCouleur) {
		if (is_numeric($idCouleur)) {
			$coulM = new CouleurManager();
			if ($coulM->getCouleurNom($idCouleur) != "") {
				
				$coul = new Couleur($idCouleur, $coulM->getCouleurNom($idCouleur));
				$this->_OCouleur = $coul;
			
			} else {
				$this->_OCouleur = null;
			}
			unset($coul);
		}
	}
	
	/**
	 * Configure le sous objet Producteur avec les informations de la base de données 
	 * (si idProducteur existe)
	 * 
	 * @param number $idProducteur
	 */
	public function setProducteurO(int $idProducteur) {
		if (is_numeric($idProducteur)) {
			$prodM = new ProducteurManager();
			$prod = new Producteur($idProducteur, 
									$prodM->getProducteurNom($idProducteur),
									$prodM->getProducteurPrenom($idProducteur),
									$prodM->getProducteurAdresse($idProducteur),
									$prodM->getProducteurCP($idProducteur),
									$prodM->getProducteurVille($idProducteur),
									$prodM->getProducteurTelephone($idProducteur),
									$prodM->getProducteurHoraire($idProducteur),
									$prodM->getProducteurWeb($idProducteur),
									$prodM->getProducteurEmail($idProducteur));
				
				$this->_OProducteur = $prod;
			unset($prod);
		}
	}
	
	/**
	 * Affecte le volume à l'objet cave
	 * 
	 * @param number $idVolume
	 */
	public function setVolumeO(int $idVolume) {
		if (is_numeric($idVolume)) {
			$VolM = new VolumeManager();
			if ($VolM->getVolumeNom($idVolume) != "") {
				
				$Vol = new Volume($idVolume, $VolM->getVolumeNom($idVolume));
				$this->_OVolume = $Vol;
			} else {
				$this->_OVolume = null;
			}
			unset($Vol);
		}
	}
	
	public function setAnnee(int $Annee) {
		if (is_numeric($Annee) && 
			/*	count($Annee)==4 && */
				$Annee >= 1970 && $Annee <= intval(date("Y"))) {
			$this->_Annee = $Annee;
		} else {
			$this->_Annee = 0;
		}
	}
	
	public function setNbBouteille(int $NbBouteille) {
		if (is_numeric($NbBouteille)) {
			$this->_NbBouteille = $NbBouteille;
		}
	}
	
	public function setAnneeConso(int $AnneeConso) {
		if (is_numeric($AnneeConso) &&
				/*count($AnneeConso)==4 &&*/
				$AnneeConso >= 1970/* && $AnneeConso <= intval(date("Y"))*/) {
					$this->_AnneeConso = $AnneeConso;
		} else {
			$this->_AnneeConso = 0;
		}
		
	}
	
	public function setReviewO(int $idReview) {
		$this->_idReview = $idReview;
	}
	
	function __destruct() {
		unset($this->_idBouteille);
		unset($this->_OProducteur);
		unset($this->_OCepage);
		unset($this->_OCouleur);
		unset($this->_OVolume);
		unset($this->_Annee);
		unset($this->_NbBouteille);
		unset($this->_AnneeConso);
		unset($this->_OReview);
	}

}

?>