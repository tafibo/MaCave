<?php
require_once 'class/class.Cepage.php';
/**
 *
 * Permet de gerer plusieurs objets de type Cepage
 *
 */
class CepageManager {

	private $_oMySql;
	const  TABLE_NAME = "d_nomcepage";

	public function __construct() {
		try {
			$this->_oMySql = $GLOBALS["_oMySql"] ;
		} catch(PDOException $e) {
			echo "Impossible de se connecter!";
			echo  $e->getTrace();
		}
	}

	public function getCepagelist() {
		
		$query = "select id_cepage, Nom_cepage from ".self::TABLE_NAME;

		$Result = $this->_oMySql->query($query) or die(trigger_error("Erreur dans l'execution de la requête :".print_r($this->_oMySql->errorInfo()), E_USER_WARNING)); 
		
		$j = 0;

		while($d = $Result->fetch()) {
			$CepageList[$j] = new Cepage($d[0], $d[1]);
			$j++;
		}
		
		return $CepageList;
		
	}

	public function getCepageNom(int $IdCepage) {
		$query = "select Nom_cepage
					from ".self::TABLE_NAME."
					where id_cepage = ".$IdCepage;
		
		$resultat = $this->_oMySql->query($query);
		return $resultat->fetch()['Nom_cepage'];
	}
	
	public function getCepageId(string $NomCepage) {
		$query = "select id_cepage
					from ".self::TABLE_NAME."
					where Nom_cepage = '".$NomCepage."'";

		$resultat = $this->_oMySql->query($query);
		return $resultat->fetch()['id_cepage'];
	}

	public function removeCepage(int $IdCepage) {
		
		$query = "delete from ".self::TABLE_NAME."
					where id_cepage = '".$IdCepage."'";
		
		return $this->_oMySql->query($query);
		/*
		if ($this->_oMySql->query($query)) {
			$query = "call updateAutoIncrement('".self::TABLE_NAME."')";
			return $this->_oMySql->query($query);
		}
		*/
	}

	public function createCepage(Cepage $Cepage) {
		$query = "insert into ".self::TABLE_NAME." (Nom_cepage) value ('".$Cepage->getCepageName()."')";
		
		return $this->_oMySql->query($query);
	}

	public function updateCepage(Cepage $Cepage) {
		$query = "update ".self::TABLE_NAME." set ".
					" Nom_cepage='".$Cepage->getCepageName()."'".
					" where id_cepage=".$Cepage->getCepageId();
		
		return $this->_oMySql->query($query);
	}

	function __destruct() {
		unset($this->_oMySql);
	}

}

?>