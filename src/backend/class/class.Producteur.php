<?php

class Producteur {

	private $_id;
	private $_Nom;
	private $_Prenom;
	private $_Adresse;
	private $_CP;
	private $_Ville;
	private $_Telephone;
	private $_Horaire;
	private $_Site_web;
	private $_Email;

	public function __construct($id = 0, $Nom = "", $Prenom = "", $Adresse = "", 
								$CP = "", $Ville = "", $Telephone = "", $Horaire = "", 
								$Site_web = "", $Email = "") {
		$this->setProducteurId($id);
		$this->setProducteurNom($Nom);
		$this->setProducteurPrenom($Prenom);
		$this->setProducteurAdresse($Adresse);
		$this->setProducteurCP($CP);
		$this->setProducteurVille($Ville);
		$this->setProducteurTelephone($Telephone);
		$this->setProducteurHoraire($Horaire);
		$this->setProducteurWeb($Site_web);
		$this->setProducteurEmail($Email);
	}

	public function getProducteurId() { return $this->_id; }
	public function getProducteurNom() { return $this->_Nom; }
	public function getProducteurPrenom() { return $this->_Prenom; }
	public function getProducteurAdresse() { return $this->_Adresse; }
	public function getProducteurCP() { return $this->_CP; }
	public function getProducteurVille() { return $this->_Ville; }
	public function getProducteurTelephone() { return $this->_Telephone; }
	public function getProducteurHoraire() { return $this->_Horaire; }
	public function getProducteurWeb() { return $this->_Site_web; }
	public function getProducteurEmail() { return $this->_Email; }

	public function setProducteurId($id) { 
		$this->_id = $id;/*
		if (is_numeric($id)) {
			$this->_id = $id;
			return true; 
		} else {
			return false;
		}*/
	}
	public function setProducteurNom($Nom) { $this->_Nom = $Nom; }
	public function setProducteurPrenom($Prenom) {$this->_Prenom = $Prenom; }
	public function setProducteurAdresse($Adresse) { $this->_Adresse = $Adresse; }
	public function setProducteurCP($CP) { 
		$this->_CP = $CP;/*
		if ( preg_match ( "/^[0-9]{5,5}$/" , $CP ) ) { 
			$this->_CP = $CP;
			return true; 
		} else {
			return false;
		}*/
	}
	public function setProducteurVille($Ville) { $this->_Ville = $Ville; }
	public function setProducteurTelephone($Telephone) { 
		$this->_Telephone = $Telephone;
		/*
		if ( preg_match ( "/^(\d\d\s){4}(\d\d)$/" ,  $Telephone) ) {
			$this->_Telephone = $Telephone;
			return true; 
		} else {
			return false;
		}
		*/
	}
	public function setProducteurHoraire($Horaire) { $this->_Horaire = $Horaire; }
	public function setProducteurWeb($Site_web) { 
		$this->_Site_web = $Site_web;/*
		if ( preg_match ( "/^(http|https|ftp):\/\/([\w]*)\.([\w]*)\.(com|net|org|biz|info|mobi|us|cc|bz|tv|ws|name|co|me|fr)(\.[a-z]{1,3})?\z/i " , $Site_web ) ) {
			$this->_Site_web = $Site_web;
			return true; 
		} else {
			return false;
		}*/
	}
	public function setProducteurEmail($Email) { 
		$this->_Email = $Email;/*
		if ( preg_match ( "/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/" , $Email) ) {
			$this->_Email = $Email; 
			return true;
		} else {
			return false;
		}*/
	}
	
	public function printout() {
		echo "id  	    : ".$this->getProducteurId().
			 "Nom 	    : ".$this->getProducteurNom().
			 "Prenom    : ".$this->getProducteurPrenom().
			 "Adresse   : ".$this->getProducteurAdresse().
			 "CP        : ".$this->getProducteurCP().
			 "Ville     : ".$this->getProducteurVille().
			 "Téléphone : ".$this->getProducteurTelephone().
		 	 "Horaire   : ".$this->getProducteurHoraire().
			 "Web	    : ".$this->getProducteurWeb().
			 "Email	    : ".$this->getProducteurEmail(); 
	}
	
	function __destruct() {
		unset($this->_id);
		unset($this->_Nom);
		unset($this->_Prenom);
		unset($this->_Adresse);
		unset($this->_CP);
		unset($this->_Ville);
		unset($this->_Telephone);
		unset($this->_Horaire);
		unset($this->_Site_web);
		unset($this->_Email);
	}
	
}
?>