<?php
require_once 'config.php';
require_once 'fonction.php';

require_once 'class/class.Volume.php'; 
require_once 'class/class.VolumeManager.php';

$id 	= isset($_POST["id"])?	(int)htmlentities($_POST["id"]):0;
$nom 	= isset($_POST["nom"])?	htmlentities($_POST["nom"]):"";
$type 	= isset($_GET["type"])? htmlentities($_GET["type"]):"";
$return	= isset($_POST["return"])?htmlentities($_POST["return"]):"";

$VolumeO = new VolumeManager();
$result = false;

switch ($type) {
	case "jsonOption":
		$VolumeListe = $VolumeO->getVolumelist();
		for($i = 0; $i < count ( $VolumeListe ); $i ++) {
			$Volume = $VolumeListe [$i];
			
			$json[$i] = array(
				"id"			=> $Volume->getVolumeId(),
				"value"			=> $Volume->getVolumeName(),
				"name"			=> "Volume"
			);		
		}
		print json_encode($json);
	break;
	case "json":
		$VolumeListe = $VolumeO->getVolumelist();
		for($i = 0; $i < count ( $VolumeListe ); $i ++) {
			$Volume = $VolumeListe [$i];

			$json[$i] = array(
				"id"			=> $Volume->getVolumeId(),
				"nom"			=> $Volume->getVolumeName()
			);		
		}
		print json_encode($json);
	break;
	case "update":
		$vol = new Volume($id, $nom);

		try {
			$result = $VolumeO->updateVolume($vol);
			//return $result;
		}  catch (Exception $e) {
			die("Query error : ".$e->getMessage());
		} 
		
	break;

	case "add":
		$vol = new Volume($id, $nom);

		try {
			$result = $VolumeO->createVolume($vol);
		}  catch (Exception $e) {

			die("Query error : ".$e->getMessage());
		} 
	break;
	case "del":
		try {
			$result = $VolumeO->removeVolume($id);
			return $result;
		}  catch (Exception $e) {

			die("Query error : ".$e->getMessage());
		}	
	break;
	}
?>