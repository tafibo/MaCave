<?php

/**
 * Permet de formater la liste des cepage sous forme d'option html
 **/
/*
function getCepageOption() {
    $cep = new CepageManager();
    $cepList = array();
    $cepList = $cep->getCepagelist();
    $CepageOption = "<select id=\"cepage\" onchange=\"OpenDiv(this.options[this.selectedIndex].value, this.id)\">";
    $CepageOption .= "<option value=\"null\"></option>";
    $CepageOption .= "<option value=\"Nouveau\">Nouveau</option>";
    for($i=0;$i<count($cepList);$i++) {
        $CepageOption .= "<option value=\"".$cepList[$i]->getCepageId()."\">".$cepList[$i]->getCepageName()."</option>";
    }
    $CepageOption .= "</select>";

    return $CepageOption;

    unset($cep);
    unset($cepList);
    unset($CepageOption);
}
*/
/**
 * Permet de formater la liste des Volumes sous forme d'option html
 **/
/*
function getVolumeOption() {
    $vol = new VolumeManager();
    $volList = $vol->getVolumelist();
    $VolumeOption = "<select id=\"volume\" onchange=\"OpenDiv(this.options[this.selectedIndex].value, this.id)\">";
    $VolumeOption .= "<option value=\"null\"></option>";
    $VolumeOption .= "<option value=\"Nouveau\">Nouveau</option>";
    for($i=0;$i<count($volList);$i++) {
        $VolumeOption .= "<option value=\"".$volList[$i]->getVolumeId()."\">".$volList[$i]->getVolumeName()."</option>";
    }
    $VolumeOption .= "</select>";

    return $VolumeOption;

    unset($vol);
    unset($volList);
    unset($VolumeOption);
}
*/
/**
 * Permet de formater la liste des Producteurs sous forme d'option html
 **/
/*
function getProducteurOption() {
    $prod = new ProducteurManager();
    $prodList = $prod->getProducteurlist();
    $ProducteurOption = "<select  id=\"producteur\" onchange=\"OpenDiv(this.options[this.selectedIndex].value, this.id)\">";
    $ProducteurOption .= "<option value=\"null\"></option>";
    $ProducteurOption .= "<option value=\"Nouveau\">Nouveau</option>";
    for($i=0;$i<count($prodList);$i++) {
        $ProducteurOption .= "<option value=\"".$prodList[$i]->getProducteurId()."\">".$prodList[$i]->getProducteurNom()."</option>";
    }
    $ProducteurOption .= "</select>";

    return $ProducteurOption;

    unset($prod);
    unset($prodList);
    unset($ProducteurOption);
}

/**
 * Permet de formater la liste des Couleurs sous forme d'option html
 **/
/*
function getCouleurOption() {
    $coul = new CouleurManager();
    $coulList = $coul->getCouleurlist();
    $CouleurOption = "<select id=\"couleur\" onchange=\"OpenDiv(this.options[this.selectedIndex].value, this.id)\">";
    $CouleurOption .= "<option value=\"null\"></option>";
    $CouleurOption .= "<option value=\"Nouveau\">Nouveau</option>";
    for($i=0;$i<count($coulList);$i++) {
        $CouleurOption .= "<option value=\"".$coulList[$i]->getCouleurId()."\">".$coulList[$i]->getCouleurName()."</option>";
    }
    $CouleurOption .= "</select>";

    return $CouleurOption;

    unset($coul);
    unset($coulList);
    unset($CouleurOption);
}
*/
function WriteMessage($message) {
	echo '<script>document.getElementById(msg).innerHTML='.$message.'</script>';
}


function wtf(){
  error_reporting(E_ALL);
  $args = func_get_args();
  $backtrace = debug_backtrace();
  $file = file($backtrace[0]['file']);
  $src  = $file[$backtrace[0]['line']-1];  // select debug($varname) line where it has been called
  $pat  = '#(.*)'.__FUNCTION__.' *?\( *?\$(.*) *?\)(.*)#i';  // search pattern for wtf(parameter)
  $arguments  = trim(preg_replace($pat, '$2', $src));  // extract arguments pattern
  $args_arr = array_map('trim', explode(',', $arguments));

  print '<style>
  div.debug {visible; clear: both; display: table; width: 100%; font-family: Courier,monospace; border: medium solid red; background-color: yellow; border-spacing: 5px; z-index: 999;}
  div.debug > div {display: unset; margin: 5px; border-spacing: 5px; padding: 5px;}
  div.debug .cell {display: inline-flex; padding: 5px; white-space: pre-wrap;}
  div.debug .left-cell {float: left; background-color: Violet;}
  div.debug .array {color: RebeccaPurple; background-color: Violet;}
  div.debug .object pre {color: DodgerBlue; background-color: PowderBlue;}
  div.debug .variable pre {color: RebeccaPurple; background-color: LightGoldenRodYellow;}
  div.debug pre {white-space: pre-wrap;}
  </style>'.PHP_EOL;
  print '<div class="debug">'.PHP_EOL;
  foreach ($args as $key => $arg) {
    print '<div><div class="left-cell cell"><b>';
    array_walk(debug_backtrace(),create_function('$a,$b','print "{$a[\'function\']}()(".basename($a[\'file\']).":{$a[\'line\']})<br> ";'));
    print '</b></div>'.PHP_EOL;
    if (is_array($arg)) {
      print '<div class="cell array"><b>'.$args_arr[$key].' = </b>';
      print_r(htmlspecialchars(print_r($arg)), ENT_COMPAT, 'UTF-8');
      print '</div>'.PHP_EOL;
    } elseif (is_object($arg)) {
      print '<div class="cell object"><pre><b>'.$args_arr[$key].' = </b>';
      print_r(htmlspecialchars(print_r(var_dump($arg))), ENT_COMPAT, 'UTF-8');
      print '</pre></div>'.PHP_EOL;
    } else {
      print '<div class="cell variable"><pre><b>'.$args_arr[$key].' = </b>&gt;';
      print_r(htmlspecialchars($arg, ENT_COMPAT, 'UTF-8').'&lt;');
      print '</pre></div>'.PHP_EOL;
    }
    print '</div>'.PHP_EOL;
  }
  print '</div>'.PHP_EOL;
}

?>