-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 24 août 2018 à 07:46
-- Version du serveur :  10.1.29-MariaDB
-- Version de PHP :  7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `macave`
--
CREATE DATABASE IF NOT EXISTS `macave` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `macave`;

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `updateAutoIncrement`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateAutoIncrement` (IN `v_table` VARCHAR(255))  BEGIN
DECLARE v_colonne varchar(255);
DECLARE v_nb integer;

SELECT COLUMN_NAME into v_colonne 
FROM INFORMATION_SCHEMA.COLUMNS 
where TABLE_NAME=v_table and EXTRA="auto_increment";

SET @s = CONCAT('SELECT MAX(', v_colonne, ') into @v_nb FROM ', v_table);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt ;

set v_nb = v_nb + 1;

SET @s = CONCAT('ALTER TABLE ', v_table, ' AUTO_INCREMENT = ', v_nb);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt ;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `d_couleur`
--

DROP TABLE IF EXISTS `d_couleur`;
CREATE TABLE `d_couleur` (
  `id_couleur` int(11) NOT NULL,
  `Nom_couleur` varchar(45) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `d_couleur`
--

INSERT INTO `d_couleur` (`id_couleur`, `Nom_couleur`) VALUES
(1, 'Rouge'),
(2, 'Champagne'),
(3, 'Blanc'),
(4, 'Ros&eacute;'),
(5, 'Cr&eacute;mant'),
(6, '1L');

-- --------------------------------------------------------

--
-- Structure de la table `d_nomcepage`
--

DROP TABLE IF EXISTS `d_nomcepage`;
CREATE TABLE `d_nomcepage` (
  `id_cepage` int(11) NOT NULL,
  `Nom_cepage` varchar(45) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `d_nomcepage`
--

INSERT INTO `d_nomcepage` (`id_cepage`, `Nom_cepage`) VALUES
(1, 'Moulin à  vent'),
(2, 'Muscat'),
(3, 'Julienas'),
(4, 'Brouilly'),
(5, 'Morgon'),
(6, 'Pinot Gris'),
(7, 'Riesling'),
(8, 'Pomerol'),
(9, '1L'),
(10, '1L'),
(11, '');

-- --------------------------------------------------------

--
-- Structure de la table `d_producteur`
--

DROP TABLE IF EXISTS `d_producteur`;
CREATE TABLE `d_producteur` (
  `id_producteur` int(11) NOT NULL,
  `Nom` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Prenom` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Adresse` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `CP` int(11) DEFAULT NULL,
  `Ville` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Telephone` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Horaire` tinyblob,
  `Site_web` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `d_producteur`
--

INSERT INTO `d_producteur` (`id_producteur`, `Nom`, `Prenom`, `Adresse`, `CP`, `Ville`, `Telephone`, `Horaire`, `Site_web`, `Email`) VALUES
(1, 'Laurent', 'Perrachon', '', 69840, 'Julienas', '0474044044', NULL, 'http://www.vinsperrachon.com', 'laurent@vinsperrachon.com'),
(2, 'Dufour', 'Michel Et Sébastien', 'la Grange Cochard', 69910, 'VILLIE MORGON', '0474691104', NULL, NULL, NULL),
(3, 'Château de Corcelles', NULL, NULL, 69220, 'Corcelles-en-Beaujolais', '0474660024', NULL, 'http://www.chateaudecorcelles.fr/', 'chateaudecorcelles@wanadoo.fr'),
(4, 'BACKERT', NULL, '24 faubourg des Vosges', 67120, 'DORLISHEIM', '0388383589', NULL, 'http://vinsalsacesbackert.com/index.htm', 'earI.backert@cegeteI.net'),
(7, '1L', '', '', 0, '', '', '', '', ''),
(8, '1L', '', '', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `d_producteur_2`
--

DROP TABLE IF EXISTS `d_producteur_2`;
CREATE TABLE `d_producteur_2` (
  `id_producteur` int(11) NOT NULL,
  `Nom` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Prenom` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Adresse` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `CP` int(11) DEFAULT NULL,
  `Ville` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Telephone` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Horaire` tinyblob,
  `Site_web` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `d_producteur_2`
--

INSERT INTO `d_producteur_2` (`id_producteur`, `Nom`, `Prenom`, `Adresse`, `CP`, `Ville`, `Telephone`, `Horaire`, `Site_web`, `Email`) VALUES
(1, 'Laurent', 'Perrachon', '', 69840, 'Julienas', '0474044044', NULL, 'http://www.vinsperrachon.com', 'laurent@vinsperrachon.com'),
(2, 'Dufour', 'Michel Et Sébastien', 'la Grange Cochard', 69910, 'VILLIE MORGON', '0474691104', NULL, NULL, NULL),
(3, 'Château de Corcelles', NULL, NULL, 69220, 'Corcelles-en-Beaujolais', '0474660024', NULL, 'http://www.chateaudecorcelles.fr/', 'chateaudecorcelles@wanadoo.fr'),
(4, 'BACKERT', NULL, '24 faubourg des Vosges', 67120, 'DORLISHEIM', '0388383589', NULL, 'http://vinsalsacesbackert.com/index.htm', 'earI.backert@cegeteI.net');

-- --------------------------------------------------------

--
-- Structure de la table `d_review`
--

DROP TABLE IF EXISTS `d_review`;
CREATE TABLE `d_review` (
  `id_review` int(11) NOT NULL,
  `review` blob,
  `reviewDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `d_volume`
--

DROP TABLE IF EXISTS `d_volume`;
CREATE TABLE `d_volume` (
  `id_volume` int(11) NOT NULL,
  `Nom_volume` varchar(45) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `d_volume`
--

INSERT INTO `d_volume` (`id_volume`, `Nom_volume`) VALUES
(1, '75cL'),
(2, '37,5cL'),
(5, '1L');

-- --------------------------------------------------------

--
-- Structure de la table `f_achat`
--

DROP TABLE IF EXISTS `f_achat`;
CREATE TABLE `f_achat` (
  `id_achat` int(11) NOT NULL,
  `NombreBouteille` int(11) DEFAULT NULL,
  `DateAchat` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `PrixAchat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `f_cave`
--

DROP TABLE IF EXISTS `f_cave`;
CREATE TABLE `f_cave` (
  `id_bouteille` int(11) NOT NULL,
  `id_producteur` int(11) NOT NULL,
  `id_cepage` int(11) NOT NULL,
  `id_couleur` int(11) NOT NULL,
  `id_volume` int(11) NOT NULL,
  `Annee` int(4) DEFAULT '1970',
  `NombreBouteille` int(11) DEFAULT NULL,
  `AnneeConso` int(4) DEFAULT NULL,
  `id_review` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `f_cave`
--

INSERT INTO `f_cave` (`id_bouteille`, `id_producteur`, `id_cepage`, `id_couleur`, `id_volume`, `Annee`, `NombreBouteille`, `AnneeConso`, `id_review`) VALUES
(1, 1, 1, 1, 1, 2009, 3, NULL, NULL),
(2, 2, 3, 1, 1, 2009, 3, NULL, NULL),
(3, 3, 3, 1, 1, 2011, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `l_achatcepage`
--

DROP TABLE IF EXISTS `l_achatcepage`;
CREATE TABLE `l_achatcepage` (
  `id_achat` int(11) DEFAULT NULL,
  `id_bouteille` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `d_couleur`
--
ALTER TABLE `d_couleur`
  ADD PRIMARY KEY (`id_couleur`);

--
-- Index pour la table `d_nomcepage`
--
ALTER TABLE `d_nomcepage`
  ADD PRIMARY KEY (`id_cepage`);

--
-- Index pour la table `d_producteur`
--
ALTER TABLE `d_producteur`
  ADD PRIMARY KEY (`id_producteur`);

--
-- Index pour la table `d_producteur_2`
--
ALTER TABLE `d_producteur_2`
  ADD PRIMARY KEY (`id_producteur`);

--
-- Index pour la table `d_review`
--
ALTER TABLE `d_review`
  ADD PRIMARY KEY (`id_review`);

--
-- Index pour la table `d_volume`
--
ALTER TABLE `d_volume`
  ADD PRIMARY KEY (`id_volume`);

--
-- Index pour la table `f_achat`
--
ALTER TABLE `f_achat`
  ADD PRIMARY KEY (`id_achat`);

--
-- Index pour la table `f_cave`
--
ALTER TABLE `f_cave`
  ADD PRIMARY KEY (`id_bouteille`),
  ADD KEY `bouteille_couleur_idx` (`id_couleur`),
  ADD KEY `bouteille_volume_idx` (`id_volume`),
  ADD KEY `bouteille_cepage_idx` (`id_cepage`),
  ADD KEY `bouteille_producteur_idx` (`id_producteur`),
  ADD KEY `bouteille_review_idx` (`id_review`);

--
-- Index pour la table `l_achatcepage`
--
ALTER TABLE `l_achatcepage`
  ADD KEY `bouteille_achat_idx` (`id_bouteille`),
  ADD KEY `achat_bouteille_idx` (`id_achat`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `d_couleur`
--
ALTER TABLE `d_couleur`
  MODIFY `id_couleur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `d_nomcepage`
--
ALTER TABLE `d_nomcepage`
  MODIFY `id_cepage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `d_producteur`
--
ALTER TABLE `d_producteur`
  MODIFY `id_producteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `d_producteur_2`
--
ALTER TABLE `d_producteur_2`
  MODIFY `id_producteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `d_review`
--
ALTER TABLE `d_review`
  MODIFY `id_review` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `d_volume`
--
ALTER TABLE `d_volume`
  MODIFY `id_volume` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `f_achat`
--
ALTER TABLE `f_achat`
  MODIFY `id_achat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `f_cave`
--
ALTER TABLE `f_cave`
  MODIFY `id_bouteille` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `f_cave`
--
ALTER TABLE `f_cave`
  ADD CONSTRAINT `bouteille_cepage` FOREIGN KEY (`id_cepage`) REFERENCES `d_nomcepage` (`id_cepage`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bouteille_couleur` FOREIGN KEY (`id_couleur`) REFERENCES `d_couleur` (`id_couleur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bouteille_producteur` FOREIGN KEY (`id_producteur`) REFERENCES `d_producteur` (`id_producteur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bouteille_review` FOREIGN KEY (`id_review`) REFERENCES `d_review` (`id_review`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bouteille_volume` FOREIGN KEY (`id_volume`) REFERENCES `d_volume` (`id_volume`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `l_achatcepage`
--
ALTER TABLE `l_achatcepage`
  ADD CONSTRAINT `achat_bouteille` FOREIGN KEY (`id_achat`) REFERENCES `f_achat` (`id_achat`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bouteille_achat` FOREIGN KEY (`id_bouteille`) REFERENCES `f_cave` (`id_bouteille`) ON DELETE NO ACTION ON UPDATE NO ACTION;


--
-- Métadonnées
--
USE `phpmyadmin`;

--
-- Métadonnées pour la table d_couleur
--

--
-- Métadonnées pour la table d_nomcepage
--

--
-- Métadonnées pour la table d_producteur
--

--
-- Métadonnées pour la table d_producteur_2
--

--
-- Métadonnées pour la table d_review
--

--
-- Métadonnées pour la table d_volume
--

--
-- Métadonnées pour la table f_achat
--

--
-- Métadonnées pour la table f_cave
--

--
-- Métadonnées pour la table l_achatcepage
--

--
-- Métadonnées pour la base de données macave
--
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
